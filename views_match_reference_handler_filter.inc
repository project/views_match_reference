<?php
/**
 * @file
 * Match reference.
 */

/**
 * Views handler to match reference fields with nodes.
 */
abstract class views_match_reference_handler_filter extends views_handler_filter {

  function can_expose() {
    return FALSE;
  }

  function admin_summary() {
    return $this->options['reference_field'];
  }

  /**
   * Define default value for master field.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['reference_table'] = array('default' => 'none');
    $options['reference_field'] = array('default' => '');
    return $options;
  }

  /**
   * Options from to ask the user for a master field.
   */
  function options_form(&$form, &$form_state) {
    if (isset($form['relationship'])) {
      $form['help'] = array(
        '#type' => 'item',
        '#value' => t('To match a reference, select the origin of the relationship as the basic relationship. The opposite relationship has the relating field referencing back to the basic relationship.'),
        '#weight' => -5,
      );

      // A whole bunch of code to figure out what relationships are valid for
      // this item.
      $view = $this->view;
      $relationships = $view->display_handler->get_option('relationships');
      $relationship_options = array('none' => t('Do not use a relationship'));
      $reference_table = $this->match_reference_table();

      foreach ($relationships as $relationship) {
        // relationships can't link back to self. But also, due to ordering,
        // relationships can only link to prior relationships.
        if ($type == 'relationship' && $id == $relationship['id']) {
          break;
        }
        $relationship_handler = views_get_handler($relationship['table'], $relationship['field'], 'relationship');
        // ignore invalid/broken relationships.
        if (empty($relationship_handler)) {
          continue;
        }

        // If this relationship is valid for this type, add it to the list.
        $data = views_fetch_data($relationship['table']);
        $base = $data[$relationship['field']]['relationship']['base'];
        if ($base == $reference_table) {
          $relationship_handler->init($view, $relationship);
          $relationship_options[$relationship['id']] = $relationship_handler->label();
        }
      }

      $form['reference_table'] = array(
        '#type' => 'select',
        '#title' => t('Opposite relationship'),
        '#options' => $relationship_options,
        '#default_value' => $this->options['reference_table'],
      );
    }
    else {
      $form['reference_table'] = array(
        '#type' => 'value',
        '#value' => 'none',
      );
    }

    // Relating field
    $reference_fields = array('' => '') + (array) $this->match_reference_fields();
    $form['reference_field'] = array(
      '#type' => 'select',
      '#title' => t('Opposite relating field'),
      '#options' => $reference_fields,
      '#default_value' => $this->options['reference_field'],
    );
  }

  /**
   * Obtain the table to match reference on.
   */
  abstract function match_reference_table();

  /**
   * Obtain the list of reference fields on the reference table.
   */
  abstract function match_reference_fields();

  /**
   * Validate reference field.
   */
  function validate() {
    if (empty($this->options['reference_field'])) {
      return array(t('The relationship field must be set.'));
    }
  }

  /**
   * Add joins to the query to synchronize the reference with the default base table.
   */
  function query() {
    $view = $this->view;

    $reference_table = NULL;
    if ($this->options['reference_table'] != 'none' && is_object($reference_relationship = $view->relationship[$this->options['reference_table']]) && !empty($reference_relationship->alias)) {
      $reference_table = $reference_relationship->alias;
    }

    // Add join query
    $base_alias = $this->query->ensure_table($this->table, $this->relationship);
    $this->match_reference_query($base_alias, $reference_table);
  }

  abstract function match_reference_query($base, $reference);
}
