<?php
/**
 * @file
 * Match node reference.
 */

/**
 * Views handler to match nodereference fields with nodes.
 */
class views_match_nodereference_handler_filter extends views_match_reference_handler_filter {

  /**
   * Node filter.
   */
  function match_reference_table() {
    return 'node';
  }

  /**
   * Node reference fields.
   */
  function match_reference_fields() {
    $fields = array();
    foreach (content_fields() as $field_name => $field) {
      if ($field['type'] == 'nodereference') {
        $field_data = content_views_field_views_data($field);
        $field_data_value = current($field_data);
        $field_data_info = content_database_info($field);
        $fields[$field_name] = $field_data_value[$field_data_info['columns']['nid']['column']]['title'];
      }
    }

    return $fields;
  }

  /**
   * Add nodereference joins to the query to match the reference with the default base table.
   */
  function match_reference_query($base, $reference) {
    // Load field
    $field = content_fields($this->options['reference_field']);
    $field_table_name = content_views_tablename($field);
    $field_data_info = content_database_info($field);

    // Obtain query names
    $reference_alias = $this->query->ensure_table($field_table_name, $reference);
    $reference_field_nid = $field_data_info['columns']['nid']['column'];

    // Join the reference field to the default (or relationship) table
    $this->query->table_queue[$reference_alias]['join']->extra = $base . '.nid = ' . $reference_alias . '.' . $reference_field_nid;
  }

}
